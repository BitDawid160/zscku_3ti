<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Sklep monopolowy - Pan Paweł</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/pricing/">

    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="./css/style2.css">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="pricing.css" rel="stylesheet">
  </head>
  <body>
    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <a href="glowna.php"><img src="baner.png" width=325px; height=112px /></a>
	  <h5 class="my-0 mr-md-auto font-weight-normal"></h5>
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark" href="onas.txt">O nas</a>
    <a class="p-2 text-dark" href="kontakt.txt">Kontakt</a>
    <a class="p-2 text-dark" href="regulamin.txt">Regulamin</a>
  </nav>
  <a class="btn btn-outline-primary" href="koszyk.php">Koszyk</a> <img src="koszyk.png" width=50px; height=30px />
</div>
<style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    
    <!-- Custom styles for this template -->
    <link href="form-validation.css" rel="stylesheet">
  </head>
  <body class="bg-light">
    <div class="container">
  <div class="py-5 text-center">
    <img class="d-block w-100" src="rzuber.png" />
    <h2>Kup więcej za mniej!</h2>
    <p class="lead">Oszczędź <b>21,37%</b> kupując 24-pak piwa RZUBER!</p>
  </div>
    <div class="card">
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <center><h4 class="my-0 font-weight-normal">Rzuber 24-pak</h4></center>
      </div>
      <div class="card-body">
		<img src="rzuber_piwo.png" width=90px height=330px style="float: left; margin-left:20%"/>
        <h1 class="card-title pricing-card-title" style="margin-top: 10%; margin-left: 51%;">59,40 PLN</h1>
          <p style="margin-left: 48%;">Produkt wyprzedany. <img src="sad_emoji.png" width=25px height=25px /></p>
      </div>
    </div>

 
</html>
