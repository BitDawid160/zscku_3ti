-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 25 Wrz 2019, 09:59
-- Wersja serwera: 10.4.6-MariaDB
-- Wersja PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `jakas`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `doctawca`
--

CREATE TABLE `doctawca` (
  `ID_dostawcy` int(10) NOT NULL,
  `Magazyn` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Adres` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Telefon` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `NIP` varchar(16) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `doctawca`
--

INSERT INTO `doctawca` (`ID_dostawcy`, `Magazyn`, `Adres`, `Telefon`, `NIP`) VALUES
(1, 'fajny', 'spokojna 32/2', '123456789', '213414'),
(2, 'super', 'czarna 14/7', '987654321', '5464643'),
(3, 'popsuty', 'biala 12/4', '234516879', '67578');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazayn`
--

CREATE TABLE `magazayn` (
  `ID_magazynu` int(10) NOT NULL,
  `Adres` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `Ulica` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Miasto` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `Kod` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `Kraj` varchar(128) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `magazayn`
--

INSERT INTO `magazayn` (`ID_magazynu`, `Adres`, `Ulica`, `Miasto`, `Kod`, `Kraj`) VALUES
(1, 'niebieska 12B', 'fioletowa', 'Nieznane', '23-321', 'Sosnowiec'),
(2, 'fiolteowa 32A', 'niebieska', 'Wawa', '25-456', 'Polska'),
(3, 'brudna 34A', 'czysta', 'Berlin', '21-321', 'Niemcy'),
(4, 'czysta 12C', 'brudna', 'Barcelona', '54-642', 'Hiszpania');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `ID_produktu` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `opis` text COLLATE utf8_polish_ci NOT NULL,
  `Kod_produktu` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `cena` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `produkt`
--

INSERT INTO `produkt` (`ID_produktu`, `nazwa`, `opis`, `Kod_produktu`, `cena`) VALUES
(1, 'Myszka', 'thrhtyrhujtfg', '1331231', 4),
(2, 'Klawiatura', 'rwterhgbfbhn', '65474423', 9.9),
(3, 'Sluchawki', 'retergvf', '534234', 4),
(4, 'Glosnik', 'ytjhnnvg', '435345', 29),
(5, 'Pilka do kosza', 'tertyr', '45423', 49),
(6, 'Pilka do siaty', 'rethgf', '67575', 59),
(7, 'Pilka do nogi', 'tyrthn', '43536', 70),
(8, 'zapach do auta', 'tyrggh', '4352', 3),
(9, 'fajna ozdoba do auta', 'htyujhun', '3242', 3),
(10, 'tymbark', 'retgr', '343242', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stany`
--

CREATE TABLE `stany` (
  `ID_czegos` int(10) NOT NULL,
  `ID_produktu` int(10) NOT NULL,
  `ID_magazynu` int(10) NOT NULL,
  `Ilosc` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `stany`
--

INSERT INTO `stany` (`ID_czegos`, `ID_produktu`, `ID_magazynu`, `Ilosc`) VALUES
(1, 4, 3, 45),
(2, 7, 2, 34),
(3, 8, 2, 42),
(4, 2, 4, 213),
(5, 0, 1, 31),
(6, 10, 1, 432),
(7, 6, 3, 13),
(8, 4, 4, 213),
(9, 9, 2, 45),
(10, 4, 2, 34);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `doctawca`
--
ALTER TABLE `doctawca`
  ADD PRIMARY KEY (`ID_dostawcy`);

--
-- Indeksy dla tabeli `magazayn`
--
ALTER TABLE `magazayn`
  ADD PRIMARY KEY (`ID_magazynu`);

--
-- Indeksy dla tabeli `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`ID_produktu`);

--
-- Indeksy dla tabeli `stany`
--
ALTER TABLE `stany`
  ADD PRIMARY KEY (`ID_czegos`),
  ADD KEY `ID_produktu` (`ID_produktu`),
  ADD KEY `ID_magazynu` (`ID_magazynu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `doctawca`
--
ALTER TABLE `doctawca`
  MODIFY `ID_dostawcy` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `magazayn`
--
ALTER TABLE `magazayn`
  MODIFY `ID_magazynu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `ID_produktu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `stany`
--
ALTER TABLE `stany`
  MODIFY `ID_czegos` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
