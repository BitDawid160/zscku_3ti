-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 25 Wrz 2019, 12:01
-- Wersja serwera: 10.4.6-MariaDB
-- Wersja PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `bazadanych`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dostawca`
--

CREATE TABLE `dostawca` (
  `ID_dostawcy` int(10) NOT NULL,
  `Magazyn` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Adres` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Telefon` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `NIP` varchar(16) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `dostawca`
--

INSERT INTO `dostawca` (`ID_dostawcy`, `Magazyn`, `Adres`, `Telefon`, `NIP`) VALUES
(1, 'magazyn1', 'uliczna 1/2', '123456789', '2137'),
(2, 'magazyn2', 'uliczna 3/4', '987654321', '5464643'),
(3, 'magazyn3', 'uliczna 21/37', '234516879', '1488');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `magazyn`
--

CREATE TABLE `magazyn` (
  `ID_magazynu` int(10) NOT NULL,
  `Adres` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `Ulica` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `Miasto` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `Kod` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `Kraj` varchar(128) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `magazyn`
--

INSERT INTO `magazyn` (`ID_magazynu`, `Adres`, `Ulica`, `Miasto`, `Kod`, `Kraj`) VALUES
(1, '31', 'szczecinska', 'szczecin', '21-370', 'Szczecin'),
(2, '32', 'pyrzycka', 'szczecin', '25-456', 'e'),
(3, '33', 'e', 'szzecin', '21-321', 'e'),
(4, '34', 'tak', 'e', '54-642', 'e');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkt`
--

CREATE TABLE `produkt` (
  `ID_produktu` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `opis` text COLLATE utf8_polish_ci NOT NULL,
  `Kod_produktu` varchar(16) COLLATE utf8_polish_ci NOT NULL,
  `cena` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `produkt`
--

INSERT INTO `produkt` (`ID_produktu`, `nazwa`, `opis`, `Kod_produktu`, `cena`) VALUES
(1, 'nissan', 'thrhtyrhujtfg', '1331231', 4),
(2, 'oepl', 'rwterhgbfbhn', '65474423', 9.9),
(3, 'golf', 'retergvf', '534234', 4),
(4, 'lada', 'ytjhnnvg', '435345', 29),
(5, 'dacia', 'tertyr', '45423', 49),
(6, 'e', 'rethgf', '67575', 59),
(7, 'obrazek ze swietym kk', 'tyrthn', '43536', 70),
(8, 'eee', 'tyrggh', '4352', 3),
(9, '2137/1488', 'htyujhun', '3242', 3),
(10, 'kebab', 'retgr', '343242', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stany`
--

CREATE TABLE `stany` (
  `ID_czegos` int(10) NOT NULL,
  `ID_produktu` int(10) NOT NULL,
  `ID_magazynu` int(10) NOT NULL,
  `Ilosc` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `stany`
--

INSERT INTO `stany` (`ID_czegos`, `ID_produktu`, `ID_magazynu`, `Ilosc`) VALUES
(1, 4, 3, 45),
(2, 7, 2, 34),
(3, 8, 2, 42),
(4, 2, 4, 213),
(5, 0, 1, 31),
(6, 10, 1, 432),
(7, 6, 3, 13),
(8, 4, 4, 213),
(9, 9, 2, 45),
(10, 4, 2, 34);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  ADD PRIMARY KEY (`ID_dostawcy`);

--
-- Indeksy dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  ADD PRIMARY KEY (`ID_magazynu`);

--
-- Indeksy dla tabeli `produkt`
--
ALTER TABLE `produkt`
  ADD PRIMARY KEY (`ID_produktu`);

--
-- Indeksy dla tabeli `stany`
--
ALTER TABLE `stany`
  ADD PRIMARY KEY (`ID_czegos`),
  ADD KEY `ID_produktu` (`ID_produktu`),
  ADD KEY `ID_magazynu` (`ID_magazynu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `dostawca`
--
ALTER TABLE `dostawca`
  MODIFY `ID_dostawcy` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `magazyn`
--
ALTER TABLE `magazyn`
  MODIFY `ID_magazynu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `produkt`
--
ALTER TABLE `produkt`
  MODIFY `ID_produktu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `stany`
--
ALTER TABLE `stany`
  MODIFY `ID_czegos` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
